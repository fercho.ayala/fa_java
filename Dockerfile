FROM openjdk:11.0.8-jdk AS build
WORKDIR /app

# Copy Gradle project
COPY settings.gradle .
COPY build.gradle .
COPY src src/
COPY gradle gradle/
COPY gradlew .

# Build Gradlew Java project.
RUN ./gradlew build

FROM openjdk:11.0.8-jre
WORKDIR /usr/src/app
COPY --from=build /app/build/libs/java_app-1.0.jar .
ENTRYPOINT ["java", "-jar", "java_app-1.0.jar"]
